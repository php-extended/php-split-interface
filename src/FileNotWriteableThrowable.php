<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use Throwable;

/**
 * FileNotWriteableThrowable class file.
 * 
 * This throwable represents a file that cannot be written.
 * 
 * @author Anastaszor
 */
interface FileNotWriteableThrowable extends Throwable
{
	
	/**
	 * Gets the path of the file that is not writeable.
	 * 
	 * @return string
	 */
	public function getFilePath() : string;
	
}
