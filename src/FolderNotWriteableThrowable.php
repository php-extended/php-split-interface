<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use Throwable;

/**
 * FolderNotWriteableThrowable class file.
 * 
 * This throwable represents a folder that cannot be written in.
 * 
 * @author Anastaszor
 */
interface FolderNotWriteableThrowable extends Throwable
{
	
	/**
	 * Gets the path of the folder that is not writeable.
	 * 
	 * @return string
	 */
	public function getFolderPath() : string;
	
}
