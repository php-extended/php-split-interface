<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use RuntimeException;
use Stringable;

/**
 * SplitInterface interface file.
 * 
 * This interface describes the method for a splitter object to split and join
 * files together.
 * 
 * @author Anastaszor
 */
interface SplitInterface extends Stringable
{
	
	/**
	 * Splits a file according to the given strategy.
	 * 
	 * @param string $sourceFileName
	 * @param string $destinationDirectory
	 * @return array<integer, string> the realpaths of the created files
	 * @throws FileNotFoundThrowable if the file is not found
	 * @throws FolderNotFoundThrowable if the folder cannot be found
	 * @throws FolderNotWriteableThrowable if the folder cannot be written
	 * @throws RuntimeException for any other stream problem
	 */
	public function fileSplit(string $sourceFileName, string $destinationDirectory) : array;
	
	/**
	 * Joins parts of a file to an unique file.
	 * 
	 * @param array<integer, string> $sourceFileNames
	 * @param string $destinationFile
	 * @return string the realpath of the created file
	 * @throws FileNotFoundThrowable if one of the file is not found
	 * @throws FileNotWriteableThrowable if the destination file cannot be 
	 *                                   written in folder
	 * @throws FolderNotFoundThrowable if the parent folder cannot be found
	 * @throws FolderNotWriteableThrowable if the folder cannot be written
	 * @throws RuntimeException for any glueing problem or stream problem
	 */
	public function fileJoin(array $sourceFileNames, string $destinationFile) : string;
	
}
